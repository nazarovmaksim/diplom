package nazarovmaxim.diplom._add_record_use_activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import nazarovmaxim.diplom.R;
import nazarovmaxim.diplom._database_tools.DBHelper;
import nazarovmaxim.diplom._select_drug_activity.SelectDrugActivity;

/**
 * Created by Назаров on 5/6/2016.
 */
public class AddRecordUseActivity extends Activity {
    private Integer selectDrugID;
    private String selectedDrug;
    private AlarmManager am;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_record_use);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        am = (AlarmManager)getSystemService(ALARM_SERVICE);

        ((TimePicker)findViewById(R.id.timePicker))
                .setIs24HourView(true);

        ((DatePicker)findViewById(R.id.datePicker)).setCalendarViewShown(false);

        ((Button)findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ne = new Intent(AddRecordUseActivity.this, SelectDrugActivity.class);
                startActivityForResult(ne,1);
            }
        });

        ((Button)findViewById(R.id.button_create)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePicker tp = (TimePicker)findViewById(R.id.timePicker);
                DatePicker dp = (DatePicker)findViewById(R.id.datePicker);
                Calendar calendar = Calendar.getInstance();
                calendar.set(dp.getYear(),dp.getMonth(),dp.getDayOfMonth(),tp.getCurrentHour(),tp.getCurrentMinute());
                Long timeToMillis = calendar.getTimeInMillis();
                DBHelper dbh = new DBHelper(getBaseContext());
                String comment = ((EditText)findViewById(R.id.editText3)).getText().toString();
                comment = comment.equals("")?"Комментарий отсутствует":comment;
                ContentValues cv = new ContentValues();
                cv.put("TIME",timeToMillis/1000);
                cv.put("DRUG_ID",selectDrugID);
                cv.put("COMMENT",comment);
                Long idNotif = dbh.insertResLong("use",null,cv);
                if(idNotif>=0){
                    AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    Intent intent = new Intent(AddRecordUseActivity.this, TimeNotification.class);
                    intent.putExtra("id",idNotif.intValue());
                    intent.putExtra("drug_name",selectedDrug);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(AddRecordUseActivity.this, 0,intent, PendingIntent.FLAG_CANCEL_CURRENT );
                    am.cancel(pendingIntent);
                    am.set(AlarmManager.RTC_WAKEUP, timeToMillis, pendingIntent);

                    dbh.close();
                    finish();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data !=null){
            selectedDrug = data.getStringExtra("info");
            selectDrugID = Integer.parseInt(data.getStringExtra("selectDrugID"));
            ((TextView)findViewById(R.id.tv_selectedDrug)).setText(selectedDrug);
            ((Button)findViewById(R.id.button)).setText("Изменить");
        }
    }
}
