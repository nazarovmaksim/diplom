package nazarovmaxim.diplom._add_record_use_activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import nazarovmaxim.diplom.R;
import nazarovmaxim.diplom._main_activity.MainActivity;

/**
 * Created by Назаров on 5/31/2016.
 */
public class TimeNotification extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
//        Notification notification = new Notification(R.drawable.icon, "Test", System.currentTimeMillis());
//        Intent intentTL = new Intent(context, MainActivity.class);
//        notification.setLatestEventInfo(context, "Test", "Do something!",PendingIntent.getActivity(context, 0, intentTL, PendingIntent.FLAG_CANCEL_CURRENT));
//        notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_book_white_48dp)
                        .setContentTitle("Дневник здоровья")
                        .setTicker("Настало ваше время!")
                        .setContentText("Настало время принять \""+intent.getStringExtra("drug_name")+"\"");
        Intent resultIntent = new Intent(context, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(intent.getIntExtra("id",0), mBuilder.build());
    }
}
