package nazarovmaxim.diplom._database_tools;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Назаров on 4/29/2016.
 */
public class DBAdapter extends SQLiteOpenHelper {

    public DBAdapter(Context context) {
        super(context, "diplom", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
