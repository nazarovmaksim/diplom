package nazarovmaxim.diplom._database_tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Назаров on 4/29/2016.
 */
public class DBHelper {
    private SQLiteDatabase db;
    private Context context;
    private static String DB_PATH;
    private static String DB_NAME = "diplom";
    private DBAdapter minDB;

    public DBHelper(Context incomingContext) {
        this.context = incomingContext;
        minDB = new DBAdapter(this.context);
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }

        if (checkDataBase()) {
            openDataBase();
        } else {
            copyDataBase();
            openDataBase();
        }
    }

    public boolean checkDataBase() {
        File file = new File(DB_PATH + DB_NAME);
        Boolean checkDB = file.exists();
        return checkDB;
    }

    public void openDataBase() {
        db = minDB.getWritableDatabase();
    }

    public void copyDataBase() {
        SQLiteDatabase asd = minDB.getWritableDatabase();
        asd.close();
        try {
            InputStream myInput = context.getAssets().open(DB_NAME+".sqlite");
            FileOutputStream myOutput = new FileOutputStream(DB_PATH+DB_NAME);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, len);
            }
            myInput.close();
            myOutput.close();
        } catch (IOException e) {
            Log.d("asdasd", "Копирование базы не удалось");
        }
    }

    public Cursor getCursorForQueryNotParams(String query){
        Cursor mCur = db.rawQuery(query,new String[]{});
        return mCur;
    }
    public Cursor getCursorForQuery(String query, String[] params){
        Cursor mCur = db.rawQuery(query,params);
        return mCur;
    }

    public Integer deleteRecord(Integer idRecord){
        Integer result = db.delete("use","_id = "+idRecord.toString(),null);
        return result;
    }

    public long insertResLong(String table, String nullColumnHack, ContentValues values) {
        return db.insert(table, nullColumnHack, values);
    }

    public Integer updateRecord(String table, ContentValues values,String where,String[] whereArgum){
        return db.update(table,values,where,whereArgum);
    }

    public void close() {
        if (db != null) {
            db.close();
        }
        minDB.close();
    }
}
