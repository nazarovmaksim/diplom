package nazarovmaxim.diplom._edit_record_use_activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import nazarovmaxim.diplom.R;
import nazarovmaxim.diplom._database_tools.DBHelper;
import nazarovmaxim.diplom._select_drug_activity.SelectDrugActivity;

public class EditRecordUseActivity extends AppCompatActivity {
    DBHelper dbh;

    private Integer _id;
    private Integer time;
    private Integer id_drug;
    private String comment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_record_use);
        dbh = new DBHelper(this);

        ((Button)findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ne = new Intent(EditRecordUseActivity.this, SelectDrugActivity.class);
                startActivityForResult(ne,1);
            }
        });

        final DatePicker dateView = (DatePicker)findViewById(R.id.datePicker);
        dateView.setCalendarViewShown(false);
        final TimePicker timeView = (TimePicker) findViewById(R.id.timePicker);
        timeView.setIs24HourView(true);
        Integer idDrug;
        String comment;

        Intent incomingIntent = getIntent();
        _id = incomingIntent.getIntExtra("id",-1);

        Cursor record = dbh.getCursorForQueryNotParams("select * from use where _id="+_id.toString());
        record.moveToFirst();
        Calendar calendar = Calendar.getInstance();
        time = record.getInt(record.getColumnIndex("TIME"));
        Long timeMillis =time.longValue()*1000;
        calendar.setTimeInMillis(timeMillis);
        dateView.updateDate(calendar.get(calendar.YEAR),calendar.get(calendar.MONTH),calendar.get(calendar.DAY_OF_MONTH));
        timeView.setCurrentHour(calendar.get(calendar.HOUR_OF_DAY));
        timeView.setCurrentMinute(calendar.get(calendar.MINUTE));
        record.close();

        Cursor getDrugName = dbh.getCursorForQueryNotParams("select METADRUG.DRUG_NAME, drug._id from (METADRUG inner join DRUG on METADRUG._id=DRUG.META_DRUG_ID) inner join USE on DRUG._id=USE.DRUG_ID where USE._id = "+_id);
        getDrugName.moveToFirst();
        id_drug=getDrugName.getInt(getDrugName.getColumnIndex("_id"));
        ((TextView)findViewById(R.id.tv_selectedDrug)).setText(getDrugName.getString(getDrugName.getColumnIndex("DRUG_NAME")));
        getDrugName.close();

        Cursor getComment = dbh.getCursorForQueryNotParams("select comment from use where _id="+_id.toString());
        getComment.moveToFirst();
        ((EditText)findViewById(R.id.editText3)).setText(getComment.getString(getComment.getColumnIndex("COMMENT")));
        getComment.close();

        ((Button)findViewById(R.id.button_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(dateView.getYear(),dateView.getMonth(),dateView.getDayOfMonth(),timeView.getCurrentHour(),timeView.getCurrentMinute());
                Long timeToMillis = calendar.getTimeInMillis();
                DBHelper dbh = new DBHelper(getBaseContext());
                String comment = ((EditText)findViewById(R.id.editText3)).getText().toString();
                comment = comment.equals("")?"Комментарий отсутствует":comment;
                ContentValues cv = new ContentValues();
                cv.put("TIME",timeToMillis/1000);
                cv.put("DRUG_ID",id_drug);
                cv.put("COMMENT",comment);
                Integer result = dbh.updateRecord("USE",cv,"_ID="+_id,null);
                finish();
//                if(idNotif>=0){
//                                                                                              сделать изменение существующего уведомления
//                    NotificationCompat.Builder mBuilder =
//                            new NotificationCompat.Builder(getBaseContext())
//                                    .setSmallIcon(R.drawable.ic_media_route_disabled_mono_dark)
//                                    .setContentTitle("My notification")
//                                    .setContentText("Hello World!");
//                    Intent resultIntent = new Intent(getBaseContext(), MainActivity.class);
//
//                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(getBaseContext());
//                    stackBuilder.addParentStack(MainActivity.class);
//                    stackBuilder.addNextIntent(resultIntent);
//                    PendingIntent resultPendingIntent =
//                            stackBuilder.getPendingIntent(
//                                    0,
//                                    PendingIntent.FLAG_UPDATE_CURRENT
//                            );
//                    mBuilder.setContentIntent(resultPendingIntent);
//                    NotificationManager mNotificationManager =
//                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                    mNotificationManager.notify(idNotif.intValue(), mBuilder.build());
//
//                    dbh.close();
//                    finish();
//                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data !=null){
            String selectedDrug = data.getStringExtra("info");
            id_drug = Integer.parseInt(data.getStringExtra("selectDrugID"));
            ((TextView)findViewById(R.id.tv_selectedDrug)).setText(selectedDrug);
            ((Button)findViewById(R.id.button)).setText("Изменить");
        }
    }
}
