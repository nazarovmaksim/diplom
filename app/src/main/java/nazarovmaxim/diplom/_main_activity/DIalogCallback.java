package nazarovmaxim.diplom._main_activity;

/**
 * Created by Назаров on 5/28/2016.
 */
public interface DIalogCallback {
    void EditRecord(Integer idRecord);
    void DeleteRecord(Integer idRecord);
}
