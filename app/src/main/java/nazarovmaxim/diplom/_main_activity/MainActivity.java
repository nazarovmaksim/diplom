package nazarovmaxim.diplom._main_activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import nazarovmaxim.diplom.R;
import nazarovmaxim.diplom._add_record_use_activity.AddRecordUseActivity;
import nazarovmaxim.diplom._database_tools.DBHelper;
import nazarovmaxim.diplom._edit_record_use_activity.EditRecordUseActivity;
import nazarovmaxim.diplom._map_activity.MapActivity;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DIalogCallback {
    DBHelper dbh;
    act_main_ListViewAdapter futureAdapter;
    ArrayList<act_main_ItemListView> arrayFuture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        dbh = new DBHelper(this);
        arrayFuture = new ArrayList<act_main_ItemListView>();
        futureAdapter = new act_main_ListViewAdapter(this, arrayFuture);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent intent = new Intent(MainActivity.this, AddRecordUseActivity.class);
                startActivity(intent);
            }
        });

        organizationInterface();
    }

    @Override
    protected void onResume() {
        loadInfoToListView();
        super.onResume();
    }

    private void organizationInterface() {
        ListView lv_future = (ListView) findViewById(R.id.lv_future);
        lv_future.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                act_main_DialogActionItemList dialog = new act_main_DialogActionItemList(getBaseContext(), MainActivity.this, view.getId());
                dialog.show(getFragmentManager(), "TAG");
            }
        });
        lv_future.setAdapter(futureAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (item.getItemId() == R.id.nav_map) {
            Log.d("asdasd", "map");
            startActivity(new Intent(this, MapActivity.class));
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void EditRecord(Integer idRecord) {
        Intent intent = new Intent(this, EditRecordUseActivity.class);
        intent.putExtra("id",idRecord);
        startActivity(intent);
    }

    @Override
    public void DeleteRecord(Integer idRecord) {
        if(dbh.deleteRecord(idRecord)==1){
            loadInfoToListView();
        }
    }

    private void loadInfoToListView(){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                arrayFuture.clear();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    String queryFuture = "select use._id,Metadrug.DRUG_NAME, use.TIME, use.COMMENT from (use inner join drug on use.DRUG_ID=drug._id) inner join metadrug on METADRUG._id=drug.META_DRUG_ID order by TIME desc";
//                    where USE.TIME > " + System.currentTimeMillis() / 1000 + "
                    Cursor cursorFuture = dbh.getCursorForQueryNotParams(queryFuture);
                    if (cursorFuture.getCount() != 0) {
                        cursorFuture.moveToFirst();
                        do {
                            act_main_ItemListView box = new act_main_ItemListView();

                            Integer id = cursorFuture.getInt(cursorFuture.getColumnIndex("_id"));
                            String unix_time = cursorFuture.getString(cursorFuture.getColumnIndex("TIME"));
                            String Nazvanie = cursorFuture.getString(cursorFuture.getColumnIndex("DRUG_NAME"));
                            Date convert = new Date(Long.parseLong(unix_time) * 1000);
                            String date = new SimpleDateFormat("dd.MM.yyyy").format(convert);
                            String time = new SimpleDateFormat("HH:mm").format(convert);
                            String comment = cursorFuture.getString(cursorFuture.getColumnIndex("COMMENT"));

                            box.id = id;
                            box.nazv = Nazvanie;
                            box.doza = comment;
                            box.time = time;
                            box.date = date;

                            arrayFuture.add(box);
                        } while (cursorFuture.moveToNext());
                    }
                    cursorFuture.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                futureAdapter.notifyDataSetChanged();
            }
        }.execute();
    }
}