package nazarovmaxim.diplom._main_activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by Назаров on 5/28/2016.
 */
public class act_main_DialogActionItemList  extends DialogFragment implements Dialog.OnClickListener{
    Context context;
    final String[] mActionsItemList = {"Редактировать","Удалить"};
    DIalogCallback callback;
    Integer IDRecord;


    public act_main_DialogActionItemList(Context context, DIalogCallback dIalogCallback,Integer id) {
        this.context = context;
        this.callback = dIalogCallback;
        this.IDRecord = id;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(mActionsItemList, this);

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case 0:
                callback.EditRecord(IDRecord);
                break;
            case 1:
                callback.DeleteRecord(IDRecord);
                break;
        }
    }
}
