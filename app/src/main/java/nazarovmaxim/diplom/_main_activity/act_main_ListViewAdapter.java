package nazarovmaxim.diplom._main_activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import nazarovmaxim.diplom.R;

/**
 * Created by Назаров on 5/16/2016.
 */
public class act_main_ListViewAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater lInf;
    private ArrayList<act_main_ItemListView> array;

    public act_main_ListViewAdapter(Context context, ArrayList<act_main_ItemListView> arrayFuture) {
        this.context = context;
        this.array = arrayFuture;
        this.lInf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        return array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInf.inflate(R.layout.act_main_list_item, parent, false);
        }

        act_main_ItemListView p = (act_main_ItemListView) getItem(position);

        ((TextView) view.findViewById(R.id.tv_nazvanie)).setText(p.nazv);
        ((TextView) view.findViewById(R.id.tv_doza)).setText(p.doza);
        ((TextView) view.findViewById(R.id.tv_time)).setText(p.time);
        ((TextView) view.findViewById(R.id.tv_date)).setText(p.date);
        view.setId(p.id);

        return view;
    }
}
