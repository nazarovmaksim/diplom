package nazarovmaxim.diplom._map_activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import nazarovmaxim.diplom.R;

/**
 * Created by Назаров on 5/16/2016.
 */
public class MapActivity extends FragmentActivity {
    SupportMapFragment mapFragment;
    GoogleMap map;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        map = mapFragment.getMap();
        if(map==null){
            finish();
        }
        init();
    }

    void init(){

    }
}
