package nazarovmaxim.diplom._select_drug_activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.ArrayList;

import nazarovmaxim.diplom.R;
import nazarovmaxim.diplom._database_tools.DBHelper;

/**
 * Created by Назаров on 5/6/2016.
 */
public class SelectDrugActivity extends Activity {
    ViewFlipper flipper;
    DBHelper dbh;
    SelectedDrugsSettings sds;
    View.OnClickListener clickListener;

    Cursor cur;
    ArrayList<act_select_drug_ItemList_infobox> masValues;
    SelectDrugActivity_ListViewAdapter adapt_ListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_drug);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        flipper = (ViewFlipper) findViewById(R.id.viewFlipper);
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        flipper.addView(li.inflate(R.layout.act_select_drug_search_options, null));
        flipper.addView(li.inflate(R.layout.act_select_drug_output, null));

        dbh = new DBHelper(this);
        sds = new SelectedDrugsSettings();
        cur = dbh.getCursorForQueryNotParams(sds.getQuery());
        cur.moveToLast();
        masValues = new ArrayList<act_select_drug_ItemList_infobox>();
        adapt_ListView = new SelectDrugActivity_ListViewAdapter(getBaseContext(), masValues);
        ((ListView) findViewById(R.id.listView)).setAdapter(adapt_ListView);

        clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button2:
                        cur = dbh.getCursorForQueryNotParams(sds.getQuery());
                        masValues.clear();
                        if (cur.getCount() != 0) {
                            cur.moveToFirst();
                            addTenItemInListView();
                        }
                        flipper.setInAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.act_select_drug_go_next_in));
                        flipper.setOutAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.act_select_drug_go_next_out));
                        flipper.showNext();
                        break;
                    case R.id.button3:
                        flipper.setInAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.act_select_drug_go_prev_in));
                        flipper.setOutAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.act_select_drug_go_prev_out));
                        flipper.showPrevious();
                        break;
                }
            }
        };

        ((Button) findViewById(R.id.button2)).setOnClickListener(clickListener);
        ((Button) findViewById(R.id.button3)).setOnClickListener(clickListener);

        organizationInterface();

    }

    private void addTenItemInListView() {
        for (int i = 0; i < 10; i++) {
            if (cur.getPosition() >= cur.getCount()) {
                break;
            }
            act_select_drug_ItemList_infobox box = new act_select_drug_ItemList_infobox();
            box.id = cur.getInt(cur.getColumnIndex("_id"));
            box.nazv = cur.getString(cur.getColumnIndex("DRUG_NAME"));
            box.group = "не известно, скоро исправим ...";
            box.formfactor = cur.getString(cur.getColumnIndex("FORM_FACTOR_NAME"));
            box.vendor = cur.getString(cur.getColumnIndex("VENDOR_NAME"));
            box.maxprice = cur.getString(cur.getColumnIndex("PRICE_MAX"));
            box.minprice = cur.getString(cur.getColumnIndex("PRICE_MIN"));
            box.lastString = cur.getString(cur.getColumnIndex("ITEMS_AMOUNT")) + " по " + cur.getString(cur.getColumnIndex("VOLUME"));
            masValues.add(box);

            cur.moveToNext();
        }
    }

    private void organizationInterface() {
        //region 1 выпадающий список - Группы лекарств
        String query1 = "select * from PHARM_CODES_INFO order by PHARM_CODE_DESC ";
        Cursor cur1 = dbh.getCursorForQueryNotParams(query1);
        cur1.moveToFirst();
        ArrayList<String> masValues1 = new ArrayList<>();
        masValues1.add("Любой");
        while (cur1.moveToNext()) {
            sds.mapGroupDrug.put(cur1.getString(cur1.getColumnIndex("PHARM_CODE_DESC")), cur1.getInt(cur1.getColumnIndex("_id")));
            masValues1.add(cur1.getString(cur1.getColumnIndex("PHARM_CODE_DESC")));
        }
        Spinner spinner1 = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<String> adapt1 = new ArrayAdapter<String>(this, R.layout.spinner_item, masValues1);
        adapt1.setDropDownViewResource(R.layout.spinner_item);
        spinner1.setAdapter(adapt1);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    String valueKey = ((TextView) view).getText().toString();
                    sds.selectedGroupDrugsID = sds.mapGroupDrug.get(valueKey);
                } else {
                    sds.selectedGroupDrugsID = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //endregion

        //region 2 выпадающий список - болезни
        String query2 = "select* from MKB_CODES_INFO order by MKB_CODE_DESC";
        Cursor cur2 = dbh.getCursorForQueryNotParams(query2);
        cur2.moveToFirst();
        ArrayList<String> masValues2 = new ArrayList<>();
        masValues2.add("Любой");
        while (cur2.moveToNext()) {
            sds.mapDisease.put(cur2.getString(cur2.getColumnIndex("MKB_CODE_DESC")), cur2.getInt(cur2.getColumnIndex("_id")));
            masValues2.add(cur2.getString(cur2.getColumnIndex("MKB_CODE_DESC")));
        }
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner3);
        ArrayAdapter<String> adapt2 = new ArrayAdapter<String>(this, R.layout.spinner_item, masValues2);
        adapt2.setDropDownViewResource(R.layout.spinner_item);
        spinner2.setAdapter(adapt2);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    String valueKey = ((TextView) view).getText().toString();
                    sds.selectedDiseaseID = sds.mapDisease.get(valueKey);
                } else {
                    sds.selectedDiseaseID = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //endregion

        //region 3 выпадающий список - формфактор
        String query3 = "select * from FORM_FACTOR order by FORM_FACTOR_NAME";
        Cursor cur3 = dbh.getCursorForQueryNotParams(query3);
        cur3.moveToFirst();
        ArrayList<String> masValues3 = new ArrayList<>();
        masValues3.add("Любой");
        while (cur3.moveToNext()) {
            sds.mapFormFactor.put(cur3.getString(cur3.getColumnIndex("FORM_FACTOR_NAME")), cur3.getInt(cur3.getColumnIndex("_id")));
            masValues3.add(cur3.getString(cur3.getColumnIndex("FORM_FACTOR_NAME")));
        }
        Spinner spinner4 = (Spinner) findViewById(R.id.spinner4);
        ArrayAdapter<String> adapt3 = new ArrayAdapter<String>(this, R.layout.spinner_item, masValues3);
        adapt3.setDropDownViewResource(R.layout.spinner_item);
        spinner4.setAdapter(adapt3);

        spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    String valueKey = ((TextView) view).getText().toString();
                    sds.selectedFormFactorID = sds.mapFormFactor.get(valueKey);
                } else {
                    sds.selectedFormFactorID = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //endregion

        //region 4 выпадающий список - производитель
        String query4 = "select * from VENDOR order by VENDOR_NAME";
        Cursor cur4 = dbh.getCursorForQueryNotParams(query4);
        cur4.moveToFirst();
        ArrayList<String> masValues4 = new ArrayList<>();
        masValues4.add("Любой");
        while (cur4.moveToNext()) {
            sds.mapVendor.put(cur4.getString(cur4.getColumnIndex("VENDOR_NAME")), cur4.getInt(cur4.getColumnIndex("_id")));
            masValues4.add(cur4.getString(cur4.getColumnIndex("VENDOR_NAME")));
        }
        Spinner spinner5 = (Spinner) findViewById(R.id.spinner5);
        ArrayAdapter<String> adapt4 = new ArrayAdapter<String>(this, R.layout.spinner_item, masValues4);
        adapt4.setDropDownViewResource(R.layout.spinner_item);
        spinner5.setAdapter(adapt4);

        spinner5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    String valueKey = ((TextView) view).getText().toString();
                    sds.selectedVendorID = sds.mapVendor.get(valueKey);
                } else {
                    sds.selectedVendorID = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        //endregion

        //region EditText
        EditText eT = (EditText) findViewById(R.id.editText);
        eT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                sds.selectedName = ((EditText) findViewById(R.id.editText)).getText().toString();
            }
        });
        //endregion

        //ListView
        ((ListView) findViewById(R.id.listView)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("selectDrugID", ((TextView) (((LinearLayout) view).findViewById(R.id.tv_id))).getText());
                intent.putExtra("info", ((TextView) (((LinearLayout) view).findViewById(R.id.tv_nazv))).getText());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        ((ListView) findViewById(R.id.listView)).setOnScrollListener(new AbsListView.OnScrollListener() {
            private static final int UPDATE_DOWN_OFFSET_IN_ITEMS = 5;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount >= totalItemCount - UPDATE_DOWN_OFFSET_IN_ITEMS) {
                    Log.d("asdasd","asd");
                    addTenItemInListView();
                    adapt_ListView.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        cur.close();
        super.onDestroy();
    }
}
