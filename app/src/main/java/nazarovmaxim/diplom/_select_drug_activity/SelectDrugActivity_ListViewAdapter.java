package nazarovmaxim.diplom._select_drug_activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import nazarovmaxim.diplom.R;
import nazarovmaxim.diplom._select_drug_activity.act_select_drug_ItemList_infobox;

/**
 * Created by Назаров on 5/16/2016.
 */
public class SelectDrugActivity_ListViewAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater lInf;
    private ArrayList<act_select_drug_ItemList_infobox> array;

    public SelectDrugActivity_ListViewAdapter(Context context, ArrayList<act_select_drug_ItemList_infobox> arrayFuture) {
        this.context = context;
        this.array = arrayFuture;
        this.lInf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public Object getItem(int position) {
        return array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInf.inflate(R.layout.act_select_drug_list_item, parent, false);
        }

        act_select_drug_ItemList_infobox p = (act_select_drug_ItemList_infobox) getItem(position);
        ((TextView)view.findViewById(R.id.tv_id)).setText(p.id.toString());
        ((TextView)view.findViewById(R.id.tv_nazv)).setText(p.nazv);
        ((TextView)view.findViewById(R.id.tv_group)).setText(p.group);
        ((TextView)view.findViewById(R.id.tv_formfactor)).setText(p.formfactor);
        ((TextView)view.findViewById(R.id.tv_vendor)).setText(p.vendor);
        ((TextView)view.findViewById(R.id.tv_maxprice)).setText(p.maxprice);
        ((TextView)view.findViewById(R.id.tv_minprice)).setText(p.minprice);
        ((TextView)view.findViewById(R.id.tv_lastString)).setText(p.lastString);

        return view;
    }
}
