package nazarovmaxim.diplom._select_drug_activity;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Назаров on 5/8/2016.
 */
public class SelectedDrugsSettings {
    public Integer selectedGroupDrugsID;
    public Map<String,Integer> mapGroupDrug;
    public Integer selectedDiseaseID;
    public Map<String,Integer> mapDisease;
    public Integer selectedFormFactorID;
    public Map<String,Integer> mapFormFactor;
    public Integer selectedVendorID;
    public Map<String,Integer> mapVendor;
    public String selectedName;

    public SelectedDrugsSettings() {
        mapGroupDrug = new Hashtable<String,Integer>();
        mapDisease = new Hashtable<String,Integer>();
        mapFormFactor = new Hashtable<String,Integer>();
        mapVendor = new Hashtable<String,Integer>();
    }
    public String getQuery(){
        String query = "select DRUG._id, DRUG.PRICE_MAX, DRUG.PRICE_MIN, DRUG.VOLUME, DRUG.MATERIAL_AMOUNT, DRUG.ITEMS_AMOUNT, FORM_FACTOR.FORM_FACTOR_NAME, VENDOR.VENDOR_NAME,METADRUG.DRUG_NAME from ((DRUG INNER JOIN FORM_FACTOR ON DRUG.FORM_FACTOR_ID=FORM_FACTOR._id) INNER JOIN VENDOR on DRUG.VENDOR_ID=VENDOR._id) INNER JOIN METADRUG on DRUG.META_DRUG_ID=METADRUG._id";
        if(selectedVendorID != null || selectedDiseaseID!=null || selectedFormFactorID!=null || selectedGroupDrugsID!=null || selectedName!=null){
            query+=" WHERE";
            boolean firstCondition=true;
            if(selectedFormFactorID!=null){
                if(firstCondition){
                    firstCondition=false;
                }else{
                    query+=" AND";
                }
                query+=" DRUG.FORM_FACTOR_ID="+selectedFormFactorID.toString();
            }
            if(selectedVendorID!=null){
                if(firstCondition){
                    firstCondition=false;
                }else{
                    query+=" AND";
                }
                query+=" DRUG.VENDOR_ID="+selectedVendorID.toString();
            }
            if(selectedName!=null){
                if(firstCondition){
                    firstCondition=false;
                }else{
                    query+=" AND";
                }
                query+=" METADRUG.DRUG_NAME LIKE '%"+selectedName+"%'";
            }
        }
        query+=" ORDER BY METADRUG.DRUG_NAME";
        return query;
    }
}
